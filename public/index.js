const fills = [
  '#e24329',
  '#e24329',
  '#e24329',
  '#fc6d26',
  '#fc6d26',
  '#fca326',
  '#fca326'
];

const length = fills.length;

let tempFirst;
let tempLast;

const logoPaths = Array.from(document.querySelectorAll('svg path'));

logoPaths.forEach((v, i) => {
  v.addEventListener('mouseover', event => {
    tempLast = fills[length - 1];
    for (let i = length - 1; i > 0; i--) {
      fills[i] = fills[i - 1];
    }
    fills[0] = tempLast;

    logoPaths.forEach((v, i) => {
      v.setAttribute('fill', fills[i]);
    });
  });
});
